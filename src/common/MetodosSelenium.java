package common;

import java.util.List;
import java.util.concurrent.TimeUnit;

import main.RunSimecTest;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MetodosSelenium {
	
	public WebDriver driver;
	public StringBuilder consola;
	
	//login
	public String loginUser;
	public String loginPass;
	public String loginUrl;
	
	{
		System.setProperty("webdriver.chrome.driver",
				"chromedriver.exe");
	}
//	{
//		this.driver = new ChromeDriver();
//	}	
	
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public StringBuilder getConsola() {
		return consola;
	}

	public void setConsola(StringBuilder consola) {
		this.consola = consola;
	}
	
	
	public void login(){
		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		this.driver.get(loginUrl);
		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		this.driver.findElement(By.id("username")).clear();
		this.driver.findElement(By.id("username")).sendKeys(loginUser);
		this.driver.findElement(By.id("password")).clear();
		this.driver.findElement(By.id("password")).sendKeys(loginPass);
		this.driver.findElement(By.className("loginButton")).click();
		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
	}

	public boolean verVisibilidadBy(By by){
		boolean visible = false;
		int intento = 1;
		do {
			if(intento == 2){
				try {
					Thread.sleep(RunSimecTest.tiempoEspera*4);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			try {
				this.driver.manage().timeouts()
						.implicitlyWait(0, TimeUnit.SECONDS); // nullify
																// implicitlyWait()
				WebDriverWait wait = new WebDriverWait(this.driver, 1);
				wait.until(ExpectedConditions.presenceOfElementLocated(by));
				this.driver.manage().timeouts()
						.implicitlyWait(10, TimeUnit.SECONDS); // reset
																// implicitlyWait
				visible = true;
				intento = 3;
			} catch (Exception e) {
				visible = false;
				intento += 1;
			}
		} while (intento <= 2);
		return visible;
	}
	
	public boolean verVisibilidadElemento(WebElement element){
		boolean visible = false;
		int intento =1;
		do{
			if(intento == 2){
				try {
					Thread.sleep(RunSimecTest.tiempoEspera*4);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try{
				this.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); //nullify implicitlyWait() 
				WebDriverWait wait = new WebDriverWait(this.driver, 1); 
				wait.until(ExpectedConditions.presenceOfElementLocated((By) element));
				this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //reset implicitlyWait
				visible = true;
				intento = 3;
			} catch (Exception e) {
				visible = false;
				intento += 1;
			} 			
		}while(intento<=2);
		return visible;
	}
	
	public boolean verVisibilidadBy(WebElement webElement){
		boolean visible = false;
		int intento = 1;
		do{
			if(intento == 2){
				try {
					Thread.sleep(RunSimecTest.tiempoEspera*4);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try{
				this.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); //nullify implicitlyWait() 
				WebDriverWait wait = new WebDriverWait(this.driver, 1); 
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id(webElement.getAttribute("id"))));
				this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //reset implicitlyWait
				visible = true;
				intento =3;
			} catch (Exception e) {
				visible = false;
				intento +=1 ;
			} 
		}while(intento <= 2);
		return visible;
	}
	
	public boolean hasClassError(By by) {
		WebElement element = this.driver.findElement(by);
	    String classes = element.getAttribute("class");
	    for (String c : classes.split(" ")) {
	        if (c.equals("ui-state-error")) {
	            return true;
	        }
	    }

	    return false;
	}
	
	public boolean hasError(String xpathInput, String xpathSpan, String mensaje){
		boolean hasError = false;
		if(verVisibilidadBy(By.xpath(xpathInput))){
            hasError = hasClassError(By.xpath(xpathInput));
            if(hasError) {        	
            	if(verVisibilidadBy(By.xpath(xpathSpan))){        		
            		System.err.println("\n" + mensaje +  this.driver.findElement(By.xpath(xpathSpan)).getText());
            		consola.append("\n" + mensaje +  this.driver.findElement(By.xpath(xpathSpan)).getText());
            	}else{
            		System.err.println("\n "+ mensaje + ": (causa desconocdida)" );
            		consola.append("\n "+ mensaje + ": (causa desconocdida)" );
            	}
            }
    	  }
		return hasError;
	}
	
	public boolean sendText(String keys, String xpath, String mensaje) throws InterruptedException{
		Thread.sleep(RunSimecTest.tiempoEspera);
		if(keys!=null && verVisibilidadBy(By.xpath(xpath))){
			this.driver.findElement(By.xpath(xpath)).sendKeys(keys);
			System.out.println("\n\t" + mensaje + ": " + keys + ", valor insertado: " + this.driver.findElement(By.xpath(xpath)).getAttribute("value"));
			consola.append("\n\t" + mensaje + ": " + keys + ", valor insertado: " + this.driver.findElement(By.xpath(xpath)).getAttribute("value"));
		}else{
			return false;
		}
		return true;
	}
	
	public boolean sendSelectByText(String xpathComboLabel, String xpathComboItems, String buscar,String mensajeExito, String mensajeError) throws InterruptedException{
		Thread.sleep(RunSimecTest.tiempoEspera);
		this.driver.findElement(By.xpath(xpathComboLabel)).click();
		WebElement dropdown=driver.findElement(By.xpath(xpathComboItems));
		List<WebElement> opciones = dropdown.findElements(By.tagName("li"));
		int contadorEstado = 0;
		
		for (WebElement opcion : opciones) {
			contadorEstado += 1;
			if(opcion.getText().equalsIgnoreCase(buscar.trim())){
				Thread.sleep(RunSimecTest.tiempoEspera);
				opcion.click();
				Thread.sleep(RunSimecTest.tiempoEspera);
				System.out.println("\n\t" + mensajeExito + ": " + buscar 
						+ ", valor seleccionado: " + this.driver.findElement(By.xpath(xpathComboLabel)).getText());
				consola.append("\n\t" + mensajeExito + ": " + buscar 
						+ ", valor seleccionado: " + this.driver.findElement(By.xpath(xpathComboLabel)).getText());
				return true;
				
			}else{
				if(contadorEstado == opciones.size()){
					System.err.println(mensajeError);
					consola.append(mensajeError);
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean sendSelectByPosition(String variable, String xpathSelecLabel, String xpathSelectOption, String mensaje) throws InterruptedException{
		Thread.sleep(RunSimecTest.tiempoEspera);
		if(verVisibilidadBy(By.xpath(xpathSelecLabel))){
			this.driver.findElement(By.xpath(xpathSelecLabel)).click();
			Thread.sleep(RunSimecTest.tiempoEspera);
			if(verVisibilidadBy(By.xpath(xpathSelectOption))){	
				String optionText = this.driver.findElement(By.xpath(xpathSelectOption)).getText();
				this.driver.findElement(By.xpath(xpathSelectOption)).click();
				System.out.println("\n\t" + mensaje + ": " + variable 
						+ ", valor seleccionado: " + optionText);
				this.consola.append("\n\t" + mensaje + ": " + variable 
						+ ", valor seleccionado: " + optionText);
			}else{
				System.err.println("\nNo se encontr� opci�n para " + variable + " en el campo " + mensaje );
				this.consola.append("\nNo se encontr� opci�n para " + variable + " en el campo " + mensaje );
				return false;
			}
		}else{
			System.err.println("\nNo se encontr�  el campo " + mensaje );
			this.consola.append("\nNo se encontr�  el campo " + mensaje );
			return false;
		}
		return true;
	}
	
	public boolean sendClickButton(String xpathButton) throws InterruptedException{
		Thread.sleep(RunSimecTest.tiempoEspera);
		if(verVisibilidadBy(this.driver.findElement(By.xpath(xpathButton)))){
			WebElement elementAceptar = this.driver.findElement(By.xpath(xpathButton));
			JavascriptExecutor executor = (JavascriptExecutor) this.driver;
			executor.executeScript("arguments[0].click();", elementAceptar);
		}else{
			return false;
		}
		return true;
	}
	
	public boolean sendClickButtonByCss(String cssSelectorButton) throws InterruptedException{
		Thread.sleep(RunSimecTest.tiempoEspera);
		if(verVisibilidadBy(this.driver.findElement(By.cssSelector(cssSelectorButton)))){
			WebElement elementAceptar = this.driver.findElement(By.cssSelector(cssSelectorButton));
			JavascriptExecutor executor = (JavascriptExecutor) this.driver;
			executor.executeScript("arguments[0].click();", elementAceptar);
		}else{
			return false;
		}
		return true;
	}
	
	
	
	public String obtenerNombreEstado(String idEstado){
		String nombreEstado = "";
		switch(idEstado){
			case "0":
				nombreEstado = "Todos los estados";
				break;
			case "1":
				nombreEstado = "AGUASCALIENTES";
				break;
			
			case "2":
				nombreEstado = "BAJA CALIFORNIA";
				break;

			case "3":
				nombreEstado = "BAJA CALIFORNIA SUR";
				break;
				
			case "4":
				nombreEstado = "CAMPECHE";
				break;
				
			case "5":
				nombreEstado = "COAHUILA";
				break;
				
			case "6":
				nombreEstado = "COLIMA";
				break;
				
			case "7":
				nombreEstado = "CHIAPAS";
				break;
				
			case "8":
				nombreEstado = "CHIHUAHUA";
				break;
				
			case "9":
				nombreEstado = "CIUDAD DE M�XICO";
				break;
				
			case "10":
				nombreEstado = "DURANGO";								
				break;
				
			case "11":
				nombreEstado = "GUANAJUATO";												
				break;
				
			case "12":
				nombreEstado = "GUERRERO";												
				break;
				
			case "13":
				nombreEstado = "HIDALGO";
				break;
				
			case "14":
				nombreEstado = "JALISCO";				
				break;
				
			case "15":
				nombreEstado = "M�XICO";				
				break;
				
			case "16":
				nombreEstado = "MICHOAC�N";								
				break;
				
			case "17":
				nombreEstado = "MORELOS";												
				break;
				
			case "18":
				nombreEstado = "NAYARIT";												
				break;
				
			case "19":
				nombreEstado = "NUEVO LE�N";																
				break;
				
			case "20":
				nombreEstado = "OAXACA";				
				break;
				
			case "21":
				nombreEstado = "PUEBLA";								
				break;
				
			case "22":
				nombreEstado = "QUER�TARO";												
				break;
				
			case "23":
				nombreEstado = "QUINTANA ROO";																
				break;
				
			case "24":
				nombreEstado = "SAN LUIS POTOS�";																				
				break;
				
			case "25":
				nombreEstado = "SINALOA";				
				break;
				
			case "26":
				nombreEstado = "SONORA";				
				break;
				
			case "27":
				nombreEstado = "TABASCO";				
				break;
				
			case "28":
				nombreEstado = "TAMAULIPAS";								
				break;
				
			case "29":
				nombreEstado = "TLAXCALA";				
				break;
				
			case "30":
				nombreEstado = "VERACRUZ";								
				break;
				
			case "31":
				nombreEstado = "YUCAT�N";								
				break;
				
			case "32":
				nombreEstado = "ZACATECAS";								
				break;
				
		}
		return nombreEstado;
	}

}
