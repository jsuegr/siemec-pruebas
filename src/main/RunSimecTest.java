package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

import common.MetodosSelenium;

public class RunSimecTest {

	public static long tiempoEspera;
	private Connection con;
	private Statement stmt;
	private ResultSet res;
	private String dbUrl;
	private String dbUser;
	private String dbPass;
	private InputStream input;
	private Properties prop;
	private String loginUrl;
	private String hostUrl;
	private String loginUser;
	private String loginPass;
	private String formIncidenciasUrl;
	private String formMecUrl;
	private String formMecUnicaUrl;
	public static String pathSaveLog;
	
	
	public static void main(String[] args) {
		try {
			new RunSimecTest();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public RunSimecTest() throws Exception{
		detectDirectory();
		getInput();
		
		//Mostrar menu
		Scanner key = new Scanner(System.in);
		String opcionPrueba;
		String opcionEstado;
		System.out.println("***************************Pruebas Automatizadas SIMEC************************\n\n");
		System.out.println("Elija la prueba que desea realizar:\n");
		System.out.println("\ta) Instalaci�n MEC (sin incidencias)");
		System.out.println("\tb) Instalaci�n �nica MEC (sin incidencias)");
		System.out.println("\tc) Instalaci�n MEC (con incidencias)");
		System.out.println("\td) Instalaci�n �nica MEC (con incidencias)");
		System.out.println("\te) Incidencias");
		System.out.println("\tf) Todas las pruebas\n");
		System.out.println("\tOtro) Salir\n");
		opcionPrueba = key.next();
		
		
		
		
		System.out.println("\n\n***************************Pruebas Automatizadas SIMEC************************\n\n");
		System.out.println("Elija el estado que se aplicar� la prueba:\n");
		System.out.println("\t0)TODOS LOS ESTADOS\n");
		if(opcionPrueba.equals("a") || opcionPrueba.equals("c")){
			System.out.println("\t1)Aguascalientes");
			System.out.println("\t2)Baja California");
			System.out.println("\t3)Baja California Sur");
			System.out.println("\t4)Campeche");
			System.out.println("\t5)Coahuila");
			System.out.println("\t6)Colima");
			System.out.println("\t8)Chiuhuahua");
			System.out.println("\t10)Durango");
			System.out.println("\t12)Guerrero");
			System.out.println("\t13)Hidalgo");
			System.out.println("\t15)M�xico");
			System.out.println("\t16)Michoacan");
			System.out.println("\t18)Nayarit");
			System.out.println("\t19)Nuevo Le�n");
			System.out.println("\t20)Oaxaca");
			System.out.println("\t22)Queretaro");
			System.out.println("\t23)Quintana Roo");
			System.out.println("\t24)San Luis Potosi");
			System.out.println("\t25)Sinaloa");
			System.out.println("\t26)Sonora");
			System.out.println("\t27)Tabasco");
			System.out.println("\t28)Tamaulipas");
			System.out.println("\t29)Tlaxcala");
			System.out.println("\t30)Veracruz");
			System.out.println("\t32)Zacatecas");
		}else if(opcionPrueba.equals("b") || opcionPrueba.equals("d")){
			System.out.println("\t7)Chiapas");
			System.out.println("\t9)Ciudad de M�xico");
			System.out.println("\t11)Guanajuato");
			System.out.println("\t14)Jalisco");
			System.out.println("\t17)Morelos");
			System.out.println("\t21)Puebla");
			System.out.println("\t31)Yucat�n");
		}else if(opcionPrueba.equals("e")){
			System.out.println("\t1)Aguascalientes");
			System.out.println("\t2)Baja California");
			System.out.println("\t3)Baja California Sur");
			System.out.println("\t4)Campeche");
			System.out.println("\t5)Coahuila");
			System.out.println("\t6)Colima");
			System.out.println("\t7)Chiapas");
			System.out.println("\t8)Chiuhuahua");
			System.out.println("\t9)Ciudad de M�xico");
			System.out.println("\t10)Durango");
			System.out.println("\t11)Guanajuato");
			System.out.println("\t12)Guerrero");
			System.out.println("\t13)Hidalgo");
			System.out.println("\t14)Jalisco");
			System.out.println("\t15)M�xico");
			System.out.println("\t16)Michoacan");
			System.out.println("\t17)Morelos");
			System.out.println("\t18)Nayarit");
			System.out.println("\t19)Nuevo Le�n");
			System.out.println("\t20)Oaxaca");
			System.out.println("\t21)Puebla");
			System.out.println("\t22)Queretaro");
			System.out.println("\t23)Quintana Roo");
			System.out.println("\t24)San Luis Potosi");
			System.out.println("\t25)Sinaloa");
			System.out.println("\t26)Sonora");
			System.out.println("\t27)Tabasco");
			System.out.println("\t28)Tamaulipas");
			System.out.println("\t29)Tlaxcala");
			System.out.println("\t30)Veracruz");
			System.out.println("\t31)Yucat�n");
			System.out.println("\t32)Zacatecas");
		}
		System.out.println("\tOTRO) SALIR\n");
		opcionEstado =key.next();
		
		try {
			int numEstado = Integer.valueOf(opcionEstado);
			if(numEstado<0 || numEstado > 32){
				System.exit(0);
			}
		} catch (Exception e) {
			System.exit(0);
		}
		
		//Obtener nombre de estado
		MetodosSelenium metodosSelenium = new MetodosSelenium();
		String nombreEstado = metodosSelenium.obtenerNombreEstado(opcionEstado);
		
		switch (opcionPrueba) {
		case "a":
			pruebaMecSinIncidentes(nombreEstado, opcionEstado);
			break;
		case "b":
			pruebaMecUnicaSinIncidentes(nombreEstado, opcionEstado);
			break;
		case "c":
			pruebaMecConIncidentes(nombreEstado, opcionEstado);
			break;
			
		case "d":
			pruebaMecUnicaConIncidentes(nombreEstado, opcionEstado);
			break;
			
		case "e":
			pruebaIncidencias(nombreEstado, opcionEstado);
			break;
		
		case "f":
			pruebaMecSinIncidentes(nombreEstado, opcionEstado);
			pruebaMecConIncidentes(nombreEstado, opcionEstado);
			pruebaMecUnicaSinIncidentes(nombreEstado, opcionEstado);
			pruebaMecUnicaConIncidentes(nombreEstado, opcionEstado);
			pruebaIncidencias(nombreEstado, opcionEstado);
			break;
		default:
			System.exit(0);
			break;
		}
		System.exit(0);
	}
	
	private void writeLog(String prueba, String estado) {
		File historial = new File(RunSimecTest.pathSaveLog + "hitorial.txt");
		File directory = new File(RunSimecTest.pathSaveLog);
		directory.mkdirs();
		if(!historial.exists()){
			try {
				historial.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
			String fecha = format.format(new Date());
			
			FileWriter fw = new FileWriter(historial,true); //the true will append the new data
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(fecha + "  - ejecuci�n de pruebas: " + prueba + " - " +  estado);//appends the string to the file
			bw.newLine();
			bw.flush();
			bw.close();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void getInput() {	
		
		this.prop = new Properties();

		try {
			this.input = new FileInputStream("config.properties");
			this.prop.load(this.input);

			//Tiempo de espera
			RunSimecTest.tiempoEspera = Long.valueOf(this.prop.getProperty("timepo_espera_pasos"));
			
			// BASE DE DATOS
			this.dbUrl = this.prop.getProperty("db_url");
			this.dbUser = this.prop.getProperty("db_user");
			this.dbPass = this.prop.getProperty("db_pass");

			//SIMEC URLS
			this.hostUrl = this.prop.getProperty("host_url");
			this.loginUrl = this.prop.getProperty("login_url");
			this.loginUser = this.prop.getProperty("login_user");
			this.loginPass = this.prop.getProperty("login_pass");
			this.formIncidenciasUrl = this.prop.getProperty("form_incidencias_url");
			this.formMecUrl = this.prop.getProperty("form_mec_url");
			this.formMecUnicaUrl = this.prop.getProperty("form_mec_unica_url");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (this.input != null) {
				try {
					this.input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void detectDirectory(){
		String pathFile = InstalacionUnicaTest.class.getProtectionDomain().getCodeSource().getLocation().getPath() ;
		String path = new File(pathFile).getParent() + "/pruebas simec/";
		
		try {
			RunSimecTest.pathSaveLog = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	 public void pruebaMecSinIncidentes(String nombreEstado, String opcionEstado) throws Exception{
		 detectDirectory();
		writeLog("Instalaci�n MEC sin incidencias", nombreEstado);
		RunSimecTest.pathSaveLog += "instalacion/sin incidencias/";
		InstalacionTest instalacionTest = new InstalacionTest(con,stmt,res,dbUrl,dbUser,dbPass,hostUrl,loginUrl,formMecUrl,loginUser,loginPass,false, opcionEstado);
		instalacionTest.load();
	 }
	 public void pruebaMecConIncidentes(String nombreEstado, String opcionEstado) throws Exception{
		 detectDirectory();
		 writeLog("Instalaci�n MEC con incidencias", nombreEstado);
			RunSimecTest.pathSaveLog += "instalacion/con incidencias/";
			InstalacionTest instalacionIncidenciasTest = new InstalacionTest(con,stmt,res,dbUrl,dbUser,dbPass,hostUrl,loginUrl,formMecUrl,loginUser,loginPass,true, opcionEstado);
			instalacionIncidenciasTest.load();
	 }
	 
	 public void pruebaMecUnicaSinIncidentes(String nombreEstado, String opcionEstado) throws Exception{
		 detectDirectory();
		 writeLog("Instalaci�n MEC �nica sin incidencias", nombreEstado);
		 RunSimecTest.pathSaveLog += "instalacion unica/sin incidencias/";
		 InstalacionUnicaTest instalacionUnicaTest = new InstalacionUnicaTest(con,stmt,res,dbUrl,dbUser,dbPass,hostUrl,loginUrl,formMecUnicaUrl,loginUser,loginPass,false, opcionEstado);
		 instalacionUnicaTest.load();		 
	 }
	 
	 public void pruebaMecUnicaConIncidentes(String nombreEstado, String opcionEstado) throws Exception{
		 detectDirectory();
		 writeLog("Instalaci�n MEC �nica con incidencias", nombreEstado);
			RunSimecTest.pathSaveLog += "instalacion unica/con incidencias/";
			InstalacionUnicaTest instalacionUnicaIncidenciasTest = new InstalacionUnicaTest(con,stmt,res,dbUrl,dbUser,dbPass,hostUrl,loginUrl,formMecUnicaUrl,loginUser,loginPass,true, opcionEstado);
			instalacionUnicaIncidenciasTest.load();
	 }
	 
	 public void pruebaIncidencias(String nombreEstado, String opcionEstado) throws Exception{
		 detectDirectory();
		 writeLog("Incidencias", nombreEstado);
			RunSimecTest.pathSaveLog += "incidencias/";
			IncidentesTest IncidentesTest= new IncidentesTest(con,stmt,res,dbUrl,dbUser,dbPass,hostUrl,loginUrl,formIncidenciasUrl,loginUser,loginPass, opcionEstado);
			IncidentesTest.load();
	 }
}
