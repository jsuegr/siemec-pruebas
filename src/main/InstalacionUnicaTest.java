package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.MetodosSelenium;

/**
 * @author josue.garcia
 *
 */
public class InstalacionUnicaTest extends MetodosSelenium{

	private Connection con;
	private Statement stmt;
	private ResultSet res;
	private String dbUrl;
	private String dbUser;
	private String dbPass;
	private String hostUrl;
	private String formUrl;
	public Date fechaReporte = Calendar.getInstance().getTime();
	public boolean primeraEjecucion;
	public int numeroRegistros;
	public int contadorRegistros;
	public int totalRegistros;
	public boolean registrarIncidencias;
	public String opcionEstado;
	

	public InstalacionUnicaTest(Connection con, Statement stmt, ResultSet res,
			String dbUrl, String dbUser, String dbPass, String hostUrl, String loginUrl, String formUrl,
			String loginUser, String loginPass, boolean registrarIncidencias, String opcionEstado) {
		super();
		this.con = con;
		this.stmt = stmt;
		this.res = res;
		this.dbUrl = dbUrl;
		this.dbUser = dbUser;
		this.dbPass = dbPass;
		this.hostUrl = hostUrl;
		this.loginUrl = loginUrl;
		this.formUrl = formUrl;
		this.loginUser = loginUser;
		this.loginPass = loginPass;
		this.registrarIncidencias = registrarIncidencias;
		this.opcionEstado = opcionEstado;
		this.driver = new ChromeDriver();
	}
	
	public void load() throws Exception {
		consola = new StringBuilder();
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
		String fecha = formato.format(this.fechaReporte); 
		new File(RunSimecTest.pathSaveLog).mkdirs();
		File txt = new File(RunSimecTest.pathSaveLog + fecha + ".txt");
		txt.createNewFile();
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			con = DriverManager.getConnection(dbUrl, dbUser, dbPass);
			stmt = con.createStatement();

			if (con != null) {
				System.out.println("Connected succesful");
			}

			getData();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			this.driver.close();
		}

	}

	private void getData() throws Exception {
		List<List<String>> registro = new ArrayList<>(); 
		List<String> idMec = new ArrayList<>();
		List<String> idEstado = new ArrayList<>();
		List<String> nombreEstado = new ArrayList<>();
		List<String> numeroMec = new ArrayList<>();
		List<String> horarioInstalacion = new ArrayList<>();
		List<String> seCae = new ArrayList<>();
		List<String> nombreSeCae = new ArrayList<>();
		List<String> presidente = new ArrayList<>();
		List<String> secretario1 = new ArrayList<>();
		List<String> secretario2 = new ArrayList<>();
		List<String> escrutador1 = new ArrayList<>();
		List<String> escrutador2 = new ArrayList<>();
		List<String> escrutador3 = new ArrayList<>();
		List<String> escrutador4 = new ArrayList<>();
		List<String> numeroObservadores = new ArrayList<>();
		List<String> pan = new ArrayList<>();
		List<String> pri = new ArrayList<>();
		List<String> prd = new ArrayList<>();
		List<String> verde = new ArrayList<>();
		List<String> pt = new ArrayList<>();
		List<String> movimientoCiudadano = new ArrayList<>();
		List<String> nuevaAlianza = new ArrayList<>();
		List<String> morena = new ArrayList<>();
		List<String> encuentroSocial = new ArrayList<>();
		List<String> bronco = new ArrayList<>();
		List<String> cifSenadores = new ArrayList<>();
		List<String> pactoSocial = new ArrayList<>();
		List<String> partidoCompromiso = new ArrayList<>();
		List<String> partidoChiapasUnido = new ArrayList<>();
		List<String> partidoPoderMover = new ArrayList<>();
		List<String> partidoSocialDemocrata = new ArrayList<>();
		List<String> partidoHumanistaMor = new ArrayList<>();
		List<String> partidoHumanistaCdmx = new ArrayList<>();
		List<String> cil = new ArrayList<>();
		//nombres columnas
		List<String> columnLabel = new ArrayList<>();

		//Login
		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		this.driver.get(loginUrl);
		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		this.driver.findElement(By.id("username")).clear();
		this.driver.findElement(By.id("username")).sendKeys(loginUser);
		this.driver.findElement(By.id("password")).clear();
		this.driver.findElement(By.id("password")).sendKeys(loginPass);
		this.driver.findElement(By.className("loginButton")).click();
		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
		
		try {
			String queryCount;
			String querySelect;
			if (opcionEstado.equals("0")) {
				queryCount = "SELECT COUNT(*) FROM PRUEBAS_INSTALACION WHERE ID_MEC = '2'";
				querySelect = "SELECT * FROM PRUEBAS_INSTALACION WHERE ID_MEC = '2'";
			}
//			if(opcionEstado.equals("9")){
//				queryCount = "SELECT COUNT(*) FROM PRUEBAS_INSTALACION WHERE ID_MEC = '2' AND NUMERO_MEC IN ('253','254','255','256')";
//				querySelect = "SELECT * FROM PRUEBAS_INSTALACION WHERE ID_MEC = '2'AND  NUMERO_MEC IN ('253','254','255','256')";
//			}
			else {
				queryCount = "SELECT COUNT(*) FROM PRUEBAS_INSTALACION WHERE ID_MEC = '2' AND ID_ESTADO = "
						+ this.opcionEstado;
				querySelect = "SELECT * FROM PRUEBAS_INSTALACION WHERE ID_MEC = '2' AND ID_ESTADO = "
						+ this.opcionEstado;
			}
		    ResultSet resContador = stmt.executeQuery(queryCount);
			while(resContador.next()){
				totalRegistros = Integer.valueOf(resContador.getString(1));				
			}
			res = stmt.executeQuery(querySelect);
			if (res != null) {
				//contador
				boolean isFirst = false;
				while (res.next()) {
					idMec.add(res.getString("ID_MEC"));
					idEstado.add(res.getString("ID_ESTADO"));
					nombreEstado.add(res.getString("NOMBRE_ESTADO"));
					numeroMec.add(res.getString("NUMERO_MEC"));
					horarioInstalacion.add( res.getString("HORARIO_INSTALACION"));
					seCae.add(res.getString("SE_CAE"));
					nombreSeCae.add(res.getString("NOMBRE_SE_CAE"));
					presidente.add(res.getString("PRESIDENTE"));
					secretario1.add(res.getString("SECRETARIO1"));
					secretario2.add(res.getString("SECRETARIO2"));
					escrutador1.add(res.getString("ESTRUTADOR1"));
					escrutador2.add(res.getString("ESCRUTADOR2"));
					escrutador3.add(res.getString("ESCRUTADOR3"));
					escrutador4.add(res.getString("ESCRUTADOR4"));
					numeroObservadores.add(res.getString("NUMERO_OBSERVADORES"));
					pan.add(res.getString("PAN"));
					pri.add(res.getString("PRI"));
					prd.add(res.getString("PRD"));
					verde.add(res.getString("VERDE"));
					pt.add(res.getString("PT"));
					movimientoCiudadano.add(res.getString("MOVIMIENTO_CIUDADANO"));
					nuevaAlianza.add(res.getString("NUEVA_ALIANZA"));
					morena.add(res.getString("MORENA"));
					encuentroSocial.add(res.getString("ENCUENTRO_SOCIAL"));
					bronco.add(res.getString("BRONCO"));
					cifSenadores.add(res.getString("CIF_SENADORES"));
					pactoSocial.add(res.getString("PACTO_SOCIAL"));
					partidoCompromiso.add(res.getString("PARTIDO_COMPROMISO"));
					partidoChiapasUnido.add(res.getString("PARTIDO_CHIAPAS_UNIDO"));
					partidoPoderMover.add(res.getString("PARTIDO_PODER_MOVER"));
					partidoSocialDemocrata.add(res.getString("PARTIDO_SOCIAL_DEMOCRATA"));
					partidoHumanistaMor.add(res.getString("PARTIDO_HUMANISTA_MOR"));
					partidoHumanistaCdmx.add(res.getString("PARTIDO_HUMANISTA_CDMX"));
					cil.add(res.getString("CIL"));
					if(!isFirst){
						isFirst = true;
						for (int j = 1; j <= res.getMetaData().getColumnCount() ;j++) {
							columnLabel.add((res.getMetaData().getColumnName(j)));
						}
					}
				}
				//agregar a la litas registro
				registro.add(idMec);
				registro.add(idEstado);
				registro.add(nombreEstado);
				registro.add(numeroMec);
				registro.add(horarioInstalacion);
				registro.add(seCae);
				registro.add(nombreSeCae);
				registro.add(presidente);
				registro.add(secretario1);
				registro.add(secretario2);
				registro.add(escrutador1);
				registro.add(escrutador2);
				registro.add(escrutador3);
				registro.add(escrutador4);
				registro.add(numeroObservadores);
				registro.add(pan);
				registro.add(pri);
				registro.add(prd);
				registro.add(verde);
				registro.add(pt);
				registro.add(movimientoCiudadano);
				registro.add(nuevaAlianza);
				registro.add(morena);
				registro.add(encuentroSocial);
				registro.add(bronco);
				registro.add(cifSenadores);
				registro.add(pactoSocial);
				registro.add(partidoCompromiso);
				registro.add(partidoChiapasUnido);
				registro.add(partidoPoderMover);
				registro.add(partidoSocialDemocrata);
				registro.add(partidoHumanistaMor);
				registro.add(partidoHumanistaCdmx);
				registro.add(cil);
				
				for(int i=0;i<totalRegistros;i++){
					//sumar contador
					contadorRegistros += 1;
					if(totalRegistros<=0){
						System.err.println("\nLa base de datos de pruebas no cuenta con registros para este estado con esta prueba");
						consola.append("\nLa base de datos de pruebas no cuenta con registros para este estado con esta prueba");
						break;
					}
					double porcentaje = (contadorRegistros * 100)/ totalRegistros;
					StringBuilder valoresbd = new StringBuilder();
					valoresbd.append("\nTotal de progreso: ").append(porcentaje).append("%\n");
					valoresbd.append("\n-------------------------------------------------------\n");
					valoresbd.append("Los valores de bd son[ ");
					for (int j = 0; j < columnLabel.size() ;j++) {
						valoresbd.append(columnLabel.get(j))
						.append(": ").append(registro.get(j).get(i)).append(", ");
					}

					valoresbd.append(" ]");
					System.out.println(valoresbd.toString());
					consola.append(valoresbd.toString());
					boolean mensajeForm;
						try {
							mensajeForm = form(idEstado.get(i), numeroMec.get(i), horarioInstalacion.get(i), seCae.get(i), nombreSeCae.get(i), presidente.get(i), secretario1.get(i), 
									secretario2.get(i), escrutador1.get(i), escrutador2.get(i), escrutador3.get(i), escrutador4.get(i), numeroObservadores.get(i), pan.get(i), pri.get(i), 
									prd.get(i), verde.get(i), pt.get(i), movimientoCiudadano.get(i), nuevaAlianza.get(i), morena.get(i), encuentroSocial.get(i), bronco.get(i), partidoHumanistaCdmx.get(i), 
									cifSenadores.get(i), pactoSocial.get(i), partidoCompromiso.get(i), partidoChiapasUnido.get(i), partidoPoderMover.get(i), partidoSocialDemocrata.get(i), 
									partidoHumanistaMor.get(i), cil.get(i));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} finally {
							// Imprimir consola
							//split consola 
							String[] consolaArray  = consola.toString().split("\n"); 
							Writer writer = null;
							
							try {
								//Asignar nombres
								SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
								String fecha = formato.format(this.fechaReporte); 
								writer = new BufferedWriter(new OutputStreamWriter(
										new FileOutputStream(RunSimecTest.pathSaveLog + fecha + ".txt"), "utf-8"));
								for(String linea : consolaArray){
									((BufferedWriter) writer).newLine();
									writer.write(linea.toString());
									((BufferedWriter) writer).newLine();
								}
							} catch (IOException ex) {
								// Report
								System.out.println(ex.getMessage());
							} finally {
								try {writer.close();} catch (Exception ex) {/*ignore*/}
							}
						}
//					}
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {	
				res.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	private boolean form(String idEstado, String numeroMec,
			String horarioInstalacion, String seCae, String nombreSeCae,
			String presidente, String secretario1, String secretario2, String escrutador1, String escrutador2, String escrutador3, String escrutador4, String numeroObservadores, 
			String pan, String pri, String prd, String verde, String pt, String movimientoCiudadano, String nuevaAlianza, String morena, String encuentroSocial, 
			String bronco, String partidoHumanistaCdmx, String cifSenadores, String pactoSocial, String partidoCompromiso, String partidoChiapasUnido, String partidoPoderMover, 
			String partidoSocialDemocrata, String partidoHumanistaMor,String cil) throws Exception {
		int intentos = 1;
		do{
			try {
				//corregir
				morena = "2";
				Thread.sleep(RunSimecTest.tiempoEspera);
				this.driver.get(hostUrl + formUrl);
				
				//entidad federativa
				//convertir idEstado a l id del combo
				String nombreEstado= obtenerNombreEstado(idEstado);
				
				
				//Estado
				if(!sendSelectByText("//*[@id=\"formInstalacionCaptura:comboEstado_label\"]", "//*[@id=\"formInstalacionCaptura:comboEstado_items\"]", nombreEstado
						,"Entidad federativa", "\n**Caso error\nEl estado "+ nombreEstado + " ya no se encuentra disponible") )
					return false;
				
				//numero MEC
				if(!sendSelectByText("//*[@id=\"formInstalacionCaptura:comboNoMEC_label\"]", "//*[@id=\"formInstalacionCaptura:comboNoMEC_items\"]", numeroMec, 
						"N�mero de MEC �nica", "\n**Caso error\nNo se encontr� n�mero MEC "  + numeroMec + " para el estado de " + nombreEstado))
					return false;
					
				
				
				//Horario Instalacion MEC
				if(!sendText(horarioInstalacion, "//*[@id=\"formInstalacionCaptura:horaInstalacion_input\"]", "Horario de instalaci�n de MEC �nica"))
					return false;
				
				//se o cae
				if(!sendSelectByPosition(seCae, "//*[@id=\"formInstalacionCaptura:seCae_label\"]", "//*[@id=\"formInstalacionCaptura:seCae_"+ Integer.valueOf(seCae) + "\"]" , "SE o CAE"))
					return false;
				

				//nombre SE O CAE
				if(!sendSelectByText("//*[@id=\"formInstalacionCaptura:nombreSECAE_label\"]", "//*[@id=\"formInstalacionCaptura:nombreSECAE_items\"]", nombreSeCae, "nombre SE o CAE", 
						"\n**Caso error\nNo se encontr� nombre SE o CAE: "  + nombreSeCae))
					return false;
				
				
				//Presidente/a
				if(!sendSelectByPosition(presidente, "//*[@id=\"formInstalacionCaptura:cargo5_label\"]", "//*[@id=\"formInstalacionCaptura:cargo5_"+ Integer.valueOf(presidente) + "\"]" 
						, "Presidente"))
					return false;

				//Secretario/a 1
				if(!sendSelectByPosition(secretario1, "//*[@id=\"formInstalacionCaptura:cargo6_label\"]", "//*[@id=\"formInstalacionCaptura:cargo6_"+ Integer.valueOf(secretario1) + "\"]" , 
						"Secretario 1"))
					return false;
				
				//Secretario/a 2
				if(!sendSelectByPosition(secretario2, "//*[@id=\"formInstalacionCaptura:cargo7_label\"]", "//*[@id=\"formInstalacionCaptura:cargo7_"+ Integer.valueOf(secretario1) + "\"]" , 
						"Secretario 2"))
					return false;
				
				//Primer Escuchador
				if(!sendSelectByPosition(escrutador1, "//*[@id=\"formInstalacionCaptura:cargo8_label\"]", "//*[@id=\"formInstalacionCaptura:cargo8_"+ Integer.valueOf(escrutador1) + "\"]" ,
						"Primer escrutador"))
					return false;
				
				//Segundo Escuchador
				if(!sendSelectByPosition(escrutador2, "//*[@id=\"formInstalacionCaptura:cargo9_label\"]", "//*[@id=\"formInstalacionCaptura:cargo9_"+ Integer.valueOf(escrutador2) + "\"]" ,
						"Segundo escrutador"))
					return false;
				
				//Tercer Escuchador
				if(!sendSelectByPosition(escrutador3, "//*[@id=\"formInstalacionCaptura:cargo10_label\"]", "//*[@id=\"formInstalacionCaptura:cargo10_"+ Integer.valueOf(escrutador3) + "\"]" ,
						"Tercer escrutador"))
					return false;
				
				//Cuarto Escuchador
				if(!sendSelectByPosition(escrutador4, "//*[@id=\"formInstalacionCaptura:cargo11_label\"]", "//*[@id=\"formInstalacionCaptura:cargo11_"+ Integer.valueOf(escrutador4) + "\"]" , 
						"Cuarto escrutador"))
					return false;
				
				//Numero de Observadores
				sendText(numeroObservadores, "//*[@id=\"formInstalacionCaptura:observadores_input\"]", "N�mero de observadores");
				
				//PARTIDOS POLITICOS
				//PAN
				sendText(pan, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_1_input\"]", "N�mero representantes PAN:");
				
				//PRI
				sendText(pri, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_2_input\"]", "N�mero representantes PRI");
				
				//PRD
				sendText(prd, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_3_input\"]", "N�mero representantes PRD:");
				
				//Partido Verde
				sendText(verde, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_5_input\"]", "N�mero representantes Partido Verde");
				
				//PT
				sendText(pt, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_4_input\"]", "N�mero representantes PT");
				
				//Movimiento ciudadano
				sendText(movimientoCiudadano, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_6_input\"]", "N�mero representantes Movimiento Ciudadano");

				//Nueva Alianza
				sendText(nuevaAlianza, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_7_input\"]", "N�mero representantes Nueva Alianza");

				//Morena
				sendText(morena, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_8_input\"]", "N�mero representantes Morena");
					
				//Encuentro Social
				sendText(encuentroSocial, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_9_input\"]", "N�mero representantes Encuentro Social");

				//Bronco
				sendText(bronco, "//*[@id=\"formInstalacionCaptura:idASOCIACION_4_11_input\"]", "N�mero representantes Bronco");

				//CIF
				sendText(cifSenadores, "//*[@id=\"formInstalacionCaptura:idASOCIACION_4_-2_input\"]", "N�mero representantes CIF");

				//Partido Pacto Social
				sendText(pactoSocial, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_21_input\"]", "N�mero representantes Partdio Pacto Social");

				//Partido Compromiso
				sendText(partidoCompromiso, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_20_input\"]", "N�mero representantes Partido Compromiso");

				//Partido Chiapas Unido
				sendText(partidoChiapasUnido, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_10_input\"]", "N�mero representantes Partido Chiapas Unido");
				
				//partido podemos mover a chiapas
				sendText(partidoPoderMover, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_11_input\"]", "N�mero representantes Partido Podemos Mover a Chiapas");

				//partido social democrata
				sendText(partidoSocialDemocrata, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_10_input\"]", "N�mero representantes Partido Social Dem�crata");

				//partido humanista morelos
				sendText(partidoHumanistaMor, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_11_input\"]", "N�mero representantes Partido Humanista Morelos");
				
				//partido humanista CDMX
				sendText(partidoHumanistaCdmx, "//*[@id=\"formInstalacionCaptura:idASOCIACION_1_12_input\"]", "N�mero representantes Partido Humanista CMDX");
				
				//CIL
				sendText(cil, "//*[@id=\"formInstalacionCaptura:idASOCIACION_4_-1_input\"]", "N�mero representantes CIL");
				
				//Boton aceptar
				this.driver.findElement(By.xpath("//*[@id=\"formInstalacionCaptura:aceptar\"]")).click();
				intentos = 5;
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("\n\n\tHubo un error. Se har� un segundo intento para este registro\n");
				this.consola.append("\n\n\tHubo un error. Se har� un segundo intento para este registro\n");
				intentos += 1;
			}
			
		}while(intentos <= 2);
		
		boolean bCasoExito;
		//Definir caso de exito o error
		Thread.sleep(RunSimecTest.tiempoEspera*4);
        bCasoExito = verVisibilidadBy(By.xpath("//span[contains(text(), 'Se realiz� correctamente el registro de la mesa')]"));
        if(bCasoExito) {
        	System.out.println("\n**Caso de �xito");
        	consola.append("\n**Caso de �xito");
        	if(registrarIncidencias){      
        		//Dar click en si
        		sendClickButtonByCss("[tabindex='41']");
        		Thread.sleep(RunSimecTest.tiempoEspera);
        		getDataIncidencia(idEstado, numeroMec);
        		this.driver.get(hostUrl + formUrl);
        	}else{
        		//Dar click en No
        		sendClickButtonByCss("[tabindex='42']");
        	}
        }
        else{        	
        	System.err.println("\n**Caso de error");
        	consola.append("\n**Caso de error");
        	

            //Ver Errores
            
	          //Horario Instalacion
	          hasError("//*[@id=\"formInstalacionCaptura:horaInstalacion_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeHoraInstalacion\"]/span[2]", "Horario de instalaci�n de la MEC �nica:");
	            
	            	
	          //Numero de observadores
	          hasError("//*[@id=\"formInstalacionCaptura:observadores_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeObservadores\"]/span[2]", "N�mero de Observadores/as Electorales:");
	           
	            
	          //PARTIDOS POLITCOS
	          //PAN
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_1_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_1\"]/span[2]", "N�mero de representantes PAN:");
	          
	          //PRI
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_2_input\"]","//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_2\"]/span[2]", "N�mero de representantes PAN:");            
	            
	          //PRD
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_3_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_3\"]/span[2]", "N�mero de representantes PRD:");
	          
	            
	          //Partido Verde
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_5_input\"]","//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_5\"]/span[2]" , "nN�mero de representantes Partido Verde:");
	            
	          //PT
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_4_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_4\"]/span[2]", "N�mero de representantes PT:");
	    	  
	          //Movimiento Ciudadano
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_6_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_5\"]/span[2]", "N�mero de representantes Movimiento Ciudadano:");
	          
	          //Nueva Alianza
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_7_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_7\"]/span[2]", "N�mero de representantes Nueva Alianza:");
	            
	          //Morena
	          hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_8_input9\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_8\"]/span[2]", "N�mero de representantes Morena:");
	          
	          //Bronco
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_4_11_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_4_11\"]/span[2]", "N�mero de representantes Bronco Independiente:");
	           
	         //CIF Senadores
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_4_-2_input\"]", "//*[@id=\"formInstalacionCaptura:idASOCIACION_4_-2_input\"]", "N�mero de representantes CIF Senadores:");
	
	         //PACTO SOCIAL PSI
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_21_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_21\"]/span[2]", "N�mero de representantes CIF Senadores:");
	    	      
	            
	          //partido compromiso
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_20_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_20\"]/span[2]", "N�mero de representantes Partido Compromiso Puebla:");
	    	  
	          //partido chiapas unido
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_10_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_10\"]/span[2]", "N�mero de representantes Partido Chiapas Unido:");
	
	         //partido podemos mover (chiapas)
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_11_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_11\"]/span[2]", "N�mero de representantes Partido Podemos Mover a Chiapas:");
	
	         //partido social democrata
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_10_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_10\"]/span[2]", "N�mero de representantes Partido Social Democrata:");
	
	         //partido humanista morelos
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_11_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_11\"]/span[2]", "N�mero de representantes Partido Humanista Morelos:");
	
	         //partido Humanista CDMX
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_1_12_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_1_12\"]/span[2]", "N�mero de representantes Partido Humanista CDMX:");
	
	         //CIL 
	         hasError("//*[@id=\"formInstalacionCaptura:idASOCIACION_4_-1_input\"]", "//*[@id=\"formInstalacionCaptura:mensajeidASOCIACION_4_-1\"]/span[2]", "N�mero de representantes CIL:");
	        
        }
        
        return true;
	}

	

	public void getDataIncidencia(String idEstado, String numeroMec){
		String idTipoIncidente = null;
		String otroIncidente = null;
		String fechaHoraIncidente = null;
		String causas = null;
		String resuelto = null;
		String fechaHoraSolucion = null;
		String descripcion = null;
		String nombreEstado = obtenerNombreEstado(idEstado);
		
		try {
			Connection conIncidencia = DriverManager.getConnection(dbUrl, dbUser, dbPass);
			Statement stmtIncidencia = conIncidencia.createStatement();
			ResultSet resIncidencia = stmtIncidencia.executeQuery("SELECT * FROM PRUEBA_INCIDENTES WHERE ID_ESTADO LIKE "+ idEstado +" AND ID_MESA = "+ numeroMec +" OFFSET 1 ROWS FETCH NEXT 1 ROWS ONLY");

			if (resIncidencia != null) {
				while (resIncidencia.next()) {
					idTipoIncidente = resIncidencia.getString(3);
					otroIncidente = resIncidencia.getString(4);
					fechaHoraIncidente = resIncidencia.getString(5);
					causas = resIncidencia.getString(6);
					resuelto = resIncidencia.getString(7);
					fechaHoraSolucion = resIncidencia.getString(8);
					descripcion = resIncidencia.getString(9);
					
					
					StringBuilder valoresbdIncidencias = new StringBuilder();
					valoresbdIncidencias.append("\tLos valores de bd para la incidencia con estado "+ nombreEstado +" y  n�mero MEC "+ numeroMec + " son[ ");
					for (int i = 1; i <= resIncidencia.getMetaData().getColumnCount() ;i++) {
						valoresbdIncidencias.append(resIncidencia.getMetaData().getColumnName(i))
						.append(": ").append(resIncidencia.getString(i)).append(", ");
					}
					valoresbdIncidencias.append(" ]");
					System.out.println(valoresbdIncidencias.toString());
					consola.append(valoresbdIncidencias.toString());
					
					
					try {
						formIncidencias(idEstado, numeroMec, idTipoIncidente, otroIncidente,
								fechaHoraIncidente, causas, resuelto,
								fechaHoraSolucion, descripcion);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				res.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
	
	private boolean formIncidencias(String idEstado, String idMesa, String idTipoIncidente,
			String otroIncidente, String fechaHoraIncidente, String causas,
			String resuelto, String fechaHoraSolucion, String descripcion) throws InterruptedException {

		
		String nombreEstado = obtenerNombreEstado(idEstado);
		
		//Tipo Incidente
		if(!sendSelectByPosition(idTipoIncidente, "//*[@id=\"formIncidentesCaptura:comboIncidentes_label\"]", "//*[@id=\"formIncidentesCaptura:comboIncidentes_"+ (Integer.valueOf(idTipoIncidente) - 8) +"\"]", 
				"Tipo de incidente"))
			return false;
		
		//Fecha Hora incidente
		sendText(fechaHoraIncidente, "//*[@id=\"formIncidentesCaptura:horaIncidente_input\"]", "Horario en que ocurri� el incidente");
		
		//Causa Incidente
		sendText(causas, "//*[@id=\"formIncidentesCaptura:textoCausasObservaciones\"]", "Causas del incidente");
		
		//Incidente resuelto 
		switch(resuelto){
			case "S":
				this.driver.findElement(By.xpath("//*[@id=\"formIncidentesCaptura:radioIncidenteResuelto\"]/div/div[1]/div/div[2]")).click();
				Thread.sleep(RunSimecTest.tiempoEspera);
				//Fecha Hora Solucion
				sendText(fechaHoraSolucion, "//*[@id=\"formIncidentesCaptura:horaSolucionIncidente_input\"]", "Horario en el que se soluciono el incidente");

				//Descripcion
				sendText(descripcion, "//*[@id=\"formIncidentesCaptura:textoSolucionObservaciones\"]", "Descripci�n breve de la soluci�n");
				break;
			case "N":
				this.driver.findElement(By.xpath("//*[@id=\"formIncidentesCaptura:radioIncidenteResuelto\"]/div/div[2]/div/div[2]")).click();
				break;
		}
		//Boton aceptar
		sendClickButtonByCss("[tabindex='41']");
		//boton no asociar otra incidencia
		boolean bCasoExito;
		//Definir caso de exito o error
		int intentosAceptar = 1;
		do{
			Thread.sleep(RunSimecTest.tiempoEspera*4);
			bCasoExito = verVisibilidadBy(By.xpath("//*[contains(text(), 'Deseas asociar otro incidente a la mesa')]"));			
			if (bCasoExito)
				intentosAceptar =3;
			else
				intentosAceptar += 1;
		}while(intentosAceptar>2);
        if(bCasoExito) {
        	System.out.println("\n\t*Caso de �xito para incidencia");
        	consola.append("\n\t*Caso de �xito para incidencia");
        	sendClickButtonByCss("[tabindex='41']");
        }
        else{        	
        	System.err.println("\n\t*Caso de error para incidencia");
        	consola.append("\n\t*Caso de error para incidencia");
        	//Ver Errores
        	 //Fecha Hora Inicidente
            hasError("//*[@id=\"formIncidentesCaptura:horaIncidente_input\"]", "//*[@id=\"formIncidentesCaptura:mensajeHoraIncidente\"]/div/ul/li/span", "Horario en que ocurri� el incidente");
            
            //Causas Observaciones
            hasError("//*[@id=\"formIncidentesCaptura:textoCausasObservaciones\"]", "//*[@id=\"formIncidentesCaptura:mensajeCausasObservaciones\"]/span", "Ingresa las causas del incidente y otras observaciones");
            
            //Fecha Hora Solucion
            hasError("//*[@id=\"formIncidentesCaptura:horaSolucionIncidente_input\"]", "//*[@id=\"formIncidentesCaptura:mensajeHoraSolucionIncidente\"]/div/ul/li/span", "Horario en que se solucion� el incidente");
            
            //Descripcion
            hasError("//*[@id=\"formIncidentesCaptura:textoSolucionObservaciones\"]", "//*[@id=\"formIncidentesCaptura:mensajeSolucion\"]/span", "Ingresa una descripci�n breve de la soluci�n");
        } 
        return true;
	}
}
