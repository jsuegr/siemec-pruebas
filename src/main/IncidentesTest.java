package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.MetodosSelenium;

/**
 * @author josue.garcia
 *
 */
public class IncidentesTest extends MetodosSelenium{

	private Connection con;
	private Statement stmt;
	private ResultSet res;
	private String dbUrl;
	private String dbUser;
	private String dbPass;
	private String hostUrl;
	private String formUrl;
	public Date fechaReporte = Calendar.getInstance().getTime();
	public int numeroRegistros;
	public int contadorRegistros;
	public int totalRegistros;
	public String opcionEstado;
	
	public IncidentesTest(Connection con, Statement stmt, ResultSet res,
			String dbUrl, String dbUser, String dbPass, String hostUrl, String loginUrl, String formUrl,
			String loginUser, String loginPass, String opcionEstado) {
		super();
		this.con = con;
		this.stmt = stmt;
		this.res = res;
		this.dbUrl = dbUrl;
		this.dbUser = dbUser;
		this.dbPass = dbPass;
		this.hostUrl = hostUrl;
		this.loginUrl = loginUrl;
		this.formUrl = formUrl;
		this.loginUser = loginUser;
		this.loginPass = loginPass;
		this.opcionEstado = opcionEstado;
		this.driver = new ChromeDriver();
	}

	public void load() throws Exception {
		//Iniciar salida
		consola = new StringBuilder();
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
		String fecha = formato.format(this.fechaReporte); 
		new File(RunSimecTest.pathSaveLog).mkdirs();
		File txt = new File(RunSimecTest.pathSaveLog + fecha + ".txt");
		txt.createNewFile();
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			con = DriverManager.getConnection(dbUrl, dbUser, dbPass);
			stmt = con.createStatement();

			if (con != null) {
				System.out.println("Connected succesful");
			}

			getData();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		this.driver.close();
	}

	private void getData() {

		String idEstado = null;
		String idMesa = null;
		String idTipoIncidente = null;
		String otroIncidente = null;
		String fechaHoraIncidente = null;
		String causas = null;
		String resuelto = null;
		String fechaHoraSolucion = null;
		String descripcion = null;

		login();

		try {
			String queryCount;
			String querySelect;
			switch(opcionEstado){
				case "0":
					queryCount = "SELECT COUNT(*) FROM PRUEBA_INCIDENTES";
					querySelect = "SELECT * FROM PRUEBA_INCIDENTES";
					break;
				default:
					queryCount = "SELECT COUNT(*) FROM PRUEBA_INCIDENTES WHERE ID_ESTADO LIKE " +  this.opcionEstado;
					querySelect = "SELECT * FROM PRUEBA_INCIDENTES WHERE ID_ESTADO LIKE " +  this.opcionEstado;
					break;
			}

			ResultSet resContador = stmt.executeQuery(queryCount);
			while(resContador.next()){
				totalRegistros = Integer.valueOf(resContador.getString(1));				
			}
			res = stmt.executeQuery(querySelect);

			if (res != null) {
				while (res.next()) {
					idEstado = res.getString(1);
					idMesa = res.getString(2);
					idTipoIncidente = res.getString(3);
					otroIncidente = res.getString(4);
					fechaHoraIncidente = res.getString(5);
					causas = res.getString(6);
					resuelto = res.getString(7);
					fechaHoraSolucion = res.getString(8);
					descripcion = res.getString(9);
					
					contadorRegistros += 1;
					if(totalRegistros<=0){
						System.err.println("\nLa base de datos de pruebas no cuenta con registros para este estado con esta prueba");
						consola.append("\nLa base de datos de pruebas no cuenta con registros para este estado con esta prueba");
						break;
					}
					double porcentaje = (contadorRegistros * 100)/ totalRegistros;
					StringBuilder valoresbd = new StringBuilder();
					valoresbd.append("\nTotal de progreso: ").append(porcentaje).append("%\n");
					valoresbd.append("\n-------------------------------------------------------\n");
					valoresbd.append("Porcentaje ");
					valoresbd.append("Los valores de bd son[ ");
					for (int i = 1; i <= res.getMetaData().getColumnCount() ;i++) {
						valoresbd.append(res.getMetaData().getColumnName(i))
						.append(": ").append(res.getString(i)).append(", ");
					}
					valoresbd.append(" ]");
					System.out.println(valoresbd.toString());
					consola.append(valoresbd.toString());
					
					
					try {
						form(idEstado, idMesa, idTipoIncidente, otroIncidente,
								fechaHoraIncidente, causas, resuelto,
								fechaHoraSolucion, descripcion);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						// Imprimir consola
						//split consola 
						String[] consolaArray  = consola.toString().split("\n"); 
						Writer writer = null;

						try {
							//Asignar nombres
							SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
							String fecha = formato.format(this.fechaReporte); 
						    writer = new BufferedWriter(new OutputStreamWriter(
						          new FileOutputStream(RunSimecTest.pathSaveLog + fecha + ".txt"), "utf-8"));
						    for(String linea : consolaArray){
						    	((BufferedWriter) writer).newLine();
								writer.write(linea.toString());
								((BufferedWriter) writer).newLine();
								
							}
						} catch (IOException ex) {
						    // Report
							System.out.println(ex.getMessage());
						} finally {
						   try {writer.close();} catch (Exception ex) {/*ignore*/}
						}
					}
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				res.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
	
	
	private boolean form(String idEstado, String idMesa, String idTipoIncidente,
			String otroIncidente, String fechaHoraIncidente, String causas,
			String resuelto, String fechaHoraSolucion, String descripcion) throws InterruptedException {
		int intentos = 1;
		do{
			try {
				//Ir a la p�gina del formulario
				this.driver.get(hostUrl + formUrl);
				String nombreEstado = obtenerNombreEstado(idEstado);

				//estado
				if(!sendSelectByText("//*[@id=\"formIncidentesCaptura:comboEstado_label\"]", "//*[@id=\"formIncidentesCaptura:comboEstado_items\"]", nombreEstado, 
						"Entidad federativa", "\nNo se encontr� estado " + nombreEstado))
					return false;
				
				//numero MEC
				if(!sendSelectByText("//*[@id=\"formIncidentesCaptura:comboMesas_label\"]", "//*[@id=\"formIncidentesCaptura:comboMesas_items\"]", idMesa
						, "N�mero de MEC", "\n**Caso error\nNo se encontro el n�mero de Mec " + idMesa+ " para el estado de " + nombreEstado))
					return false;
				
				//Tipo Incidente
//				if(!sendSelectByPosition(idTipoIncidente, "//*[@id=\"formIncidentesCaptura:comboIncidentes_label\"]", "//*[@id=\"formIncidentesCaptura:comboIncidentes_"+ (Integer.valueOf(idTipoIncidente) - 8) +"\"]", 
//						"Tipo de incidente"))
//					return false;
//				
				if(!sendSelectByText("//*[@id=\"formIncidentesCaptura:comboIncidentes_label\"]", "//*[@id=\"formIncidentesCaptura:comboIncidentes_items\"]", "Otro", "Otro", ""))
				return false;
				
				//Fecha Hora incidente
				if(!sendText(fechaHoraIncidente, "//*[@id=\"formIncidentesCaptura:horaIncidente_input\"]", "Horario en que ocurri� el incidente"))
					return false;
				
				//Causa Incidente
				if(!sendText(causas, "//*[@id=\"formIncidentesCaptura:textoCausasObservaciones\"]", "Causas del incidente"))
					return false;
				////*[@id="formIncidentesCaptura:textoCausasObservaciones"]
				////*[@id="formIncidentesCaptura:tituloOtros"]
				if(!sendText(causas, "//*[@id=\"formIncidentesCaptura:tituloOtros\"]", "Causas del incidente de tipo otros"))
					return false;
				
				//Incidente resuelto 
				switch(resuelto){
					case "S":
						this.driver.findElement(By.xpath("//*[@id=\"formIncidentesCaptura:radioIncidenteResuelto\"]/div/div[1]/div/div[2]")).click();
						Thread.sleep(RunSimecTest.tiempoEspera);
						//Fecha Hora Solucion
						sendText(fechaHoraSolucion, "//*[@id=\"formIncidentesCaptura:horaSolucionIncidente_input\"]", "Horario en el que se soluciono el incidente");

						//Descripcion
						sendText(descripcion, "//*[@id=\"formIncidentesCaptura:textoSolucionObservaciones\"]", "Descripci�n breve de la soluci�n");
						break;
					case "N":
						this.driver.findElement(By.xpath("//*[@id=\"formIncidentesCaptura:radioIncidenteResuelto\"]/div/div[2]/div/div[2]")).click();
						break;
				}
				intentos = 5;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.err.println("\n\n\tHubo un error. Se har� un segundo intento para este registro\n");
				this.consola.append("\n\n\tHubo un error. Se har� un segundo intento para este registro\n");
				intentos += 1;
			}
		}while(intentos <=2);
		
		Thread.sleep(RunSimecTest.tiempoEspera);
	
		//Boton aceptar
		this.driver.findElement(By.xpath("//*[@id=\"formIncidentesCaptura:BotonAceptar\"]")).click();
		
		
		boolean bCasoExito, bFechaHoraIncidente, bCausas,  bFechaHoraSolucion, bDescripcion;
		//Definir caso de exito o error
		Thread.sleep(RunSimecTest.tiempoEspera);
        bCasoExito = verVisibilidadBy(By.xpath("//*[contains(text(), 'El registro se ha guardado correctamente')]"));
        if(bCasoExito) {
        	System.out.println("\n**Caso de �xito");
        	consola.append("\n**Caso de �xito");
        }
        else{        	
        	System.err.println("\n**Caso de error");
        	consola.append("\n**Caso de error");
        	//Ver Errores
        	verErrores();
        } 
        
        return true;
	}

	public void verErrores(){
        //Fecha Hora Inicidente
        hasError("//*[@id=\"formIncidentesCaptura:horaIncidente_input\"]", "//*[@id=\"formIncidentesCaptura:mensajeHoraIncidente\"]/div/ul/li/span", "Horario en que ocurri� el incidente");
        
        //Causas Observaciones
        hasError("//*[@id=\"formIncidentesCaptura:textoCausasObservaciones\"]", "//*[@id=\"formIncidentesCaptura:mensajeCausasObservaciones\"]/span", "Ingresa las causas del incidente y otras observaciones");
        
        //Fecha Hora Solucion
        hasError("//*[@id=\"formIncidentesCaptura:horaSolucionIncidente_input\"]", "//*[@id=\"formIncidentesCaptura:mensajeHoraSolucionIncidente\"]/div/ul/li/span", "Horario en que se solucion� el incidente");
        
        //Descripcion
        hasError("//*[@id=\"formIncidentesCaptura:textoSolucionObservaciones\"]", "//*[@id=\"formIncidentesCaptura:mensajeSolucion\"]/span", "Ingresa una descripci�n breve de la soluci�n");
	}

}
